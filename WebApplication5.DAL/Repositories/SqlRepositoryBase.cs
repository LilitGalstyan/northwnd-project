﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Abstractions.Repositories;

namespace WebApplication5.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T>
        where T : class
    {
        private readonly NORTHWNDContext Context;

        public SqlRepositoryBase(NORTHWNDContext dbContext)
        {
            Context = dbContext;
        }
        public T Add(T entity)
        {
            Context.Set<T>().Add(entity);
            return entity;
        }
        public IEnumerable<T> AddRange(IEnumerable<T> entities)
        {
            Context.Set<T>().AddRange(entities);
            return entities;
        }
        public async Task<IList<T>> FindAsync(Expression<Func<T, bool>> predicate, int skip = 0, int? take = null)
        {
            var query = Context.Set<T>().Where(predicate).Skip(skip);
            if (take.HasValue)
                query = query.Take(take.Value);
            return await query.ToListAsync();
        }

        public async Task<IList<TProp>> FindAndSelectAsync<TProp>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProp>> selectProperty, int skip = 0, int? take = null)
        {
            var query = Context.Set<T>().Where(predicate).Skip(skip);
            if (take.HasValue)
                query = query.Take(take.Value);
            return await query.Select(selectProperty).ToListAsync();
        }

        public async Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<IList<T>> FindSortedAsync<TSr>(Expression<Func<T, bool>> predicate, Expression<Func<T, TSr>> sortBy, int skip = 0, int? take = null)
        {
            var query = Context.Set<T>().Where(predicate).OrderBy(sortBy).Skip(skip);
            if (take.HasValue)
                query = query.Take(take.Value);
            return await query.ToListAsync();
        }

        public void Remove(T entity)
        {
            var dbEntityEntry = Context.Entry(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Remove(entity);
            }
        }

        public virtual void Update(T entity)
        {
            var dbEntityEntry = Context.Entry(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public virtual void UpdateRange(IEnumerable<T> entities)
        {
            Context.Set<T>().UpdateRange(entities);
        }

        public async Task<IList<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = Context.Set<T>();
            return includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }


        private IQueryable<T> ThenInclude(params Expression<Func<T, object>>[] includeProperties)
        {
            //Review :: Use This In .Net Core
            //IQueryable<T> query = _dbSet.AsNoTracking();
            //return includeProperties.Aggregate(query, (current, includeProperty) => current.ThenInclude(includeProperty));

            IQueryable<T> query = Context.Set<T>();
            return includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public int GetCount(Expression<Func<T, bool>> filter = null)
        {
            if (filter != null)
                return Context.Set<T>().Where(filter).Count();
            else
                return Context.Set<T>().Count();
        }

        public async Task<int> GetCountAsync(Expression<Func<T, bool>> filter = null)
        {
            if (filter != null)
                return await Context.Set<T>().CountAsync(filter);
            return await Context.Set<T>().CountAsync();
        }

        public bool IsAny(Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
                return Context.Set<T>().Any(predicate);
            return Context.Set<T>().Any();
        }
        public async Task<bool> IsAnyAsync(Expression<Func<T, bool>> predicate)
        {
            if (predicate != null)
                return await Context.Set<T>().AnyAsync(predicate);
            return await Context.Set<T>().AnyAsync();
        }
        public void FilterAsync(Expression<Func<T, bool>> predicate)
        {
            var entities = Context.Set<T>().Where(predicate);

            foreach (var entity in entities)
            {
                Context.Entry(entity).State = EntityState.Deleted;
            }
        }

        public async Task<T> GetSingleWithInclude(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include)
        {
            return await AllIncluding(predicate, include).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> AllIncludingAsync(Expression<Func<T, bool>> whereProperties, params Expression<Func<T, object>>[] includeProperties)
        {
            return await AllIncluding(whereProperties, includeProperties).ToListAsync();
        }

        private IQueryable<T> AllIncluding(Expression<Func<T, bool>> whereProperties, params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = Context.Set<T>();
            if (whereProperties != null)
                query = query.Where(whereProperties);

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public async Task<IList<TProp>> GetAndSelectSingle<TProp>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProp>> selectProperty)
        {
            return await Context.Set<T>().Where(predicate).Select(selectProperty).ToListAsync();
        }

       /* public virtual async Task<IFilterResult<T>> FilterAsync(IAsyncSqlFilter<T> filterBase)
        {
            return await filterBase.FilterAsync(Context.Set<T>());
        }*/

        public bool IsAll(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().All(predicate);
        }

        public Task<bool> IsAllAsync(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().AllAsync(predicate);
        }

        public Task<TResult> MinAsync<TResult>(Expression<Func<T, bool>> condition, Expression<Func<T, TResult>> predicate)
        {
            return Context.Set<T>().Where(condition).MinAsync(predicate);
        }

        public TResult Min<TResult>(Expression<Func<T, bool>> condition, Expression<Func<T, TResult>> predicate)
        {
            return Context.Set<T>().Where(condition).Min(predicate);
        }

        public void RemoveWhere(Expression<Func<T, bool>> predicate)
        {
            var entities = Context.Set<T>().Where(predicate);

            Context.Set<T>().RemoveRange(entities);
        }


    }
}

    
