﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication5.DAL.Repositories
{
    public class OrderRepository:SqlRepositoryBase<Order>, IOrderRepository
    {
        private readonly NORTHWNDContext _context;
        public OrderRepository(NORTHWNDContext dbcontext) : base(dbcontext)
        {
            
        }

        public IEnumerable<OrderFilterModel> HighFreightCharges()
        {
            var HighFreight = _context.Orders
                         .GroupBy(g => g.ShipCountry)
                         .Select(g => new { g.Key, AverageFreight = g.Average(g => g.Freight) })
                         .OrderByDescending(g => g.AverageFreight)
                         .ToList();
            return (IEnumerable<OrderFilterModel>)HighFreight;
        }

        public IEnumerable<OrderFilterModel> HighFreightChargesLastYear()
        {
            var HfchLastYear = _context.Orders
                       .Where(g => g.OrderDate.Value.Year == 2000)
                       .GroupBy(g => g.ShipCountry)
                       .Select(g => g.Key)
                       .Take(3)
                       .ToList();
            return (IEnumerable<OrderFilterModel>)HfchLastYear;
        }
    }
}
