﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;

namespace WebApplication5.DAL.Repositories
{
    public class ProductRepository : SqlRepositoryBase<Product>, IProductRepository
    {
        private readonly NORTHWNDContext _context;
        public ProductRepository(NORTHWNDContext dbcontext) : base(dbcontext)
        {
           
        }
        public IEnumerable<Product> GetTotalProductsInEachCategory()
        {
            var g = _context.Products.Join(_context.Categories,
                g => g.CategoryId,
                c => c.CategoryId,
                (g, c) => new
                {
                    CategoryName = c.CategoryName,
                    CategoryId = c.CategoryId,
                    ProductId = g.ProductId
                })
                .GroupBy(c => new { c.CategoryName, c.CategoryId })
                .Select(g => new { g.Key.CategoryName, TotalProducts = g.Count() })
                .OrderByDescending(g => g.TotalProducts)
                .ToList();
            return ((IEnumerable<Product>)g);
        }
        public IEnumerable<Product> GetReorderedProducts()
        {
            var ReorderedProducts = _context.Products
                    .Where(g => g.UnitsInStock < g.ReorderLevel)
                    .OrderBy(g => g.ProductId)
                    .Select(g => new { g.ProductName, g.UnitsInStock, g.ReorderLevel })
                    .ToList();

            return ((IEnumerable<Product>)ReorderedProducts);
        }
        public IEnumerable<Product> GetReordered()
        {
            var reOrd = _context.Products
                .Where(g => g.UnitsInStock + g.UnitsOnOrder < g.ReorderLevel)
                    .OrderBy(g => g.ProductId)
                    .Select(g => new { g.ProductName, g.UnitsInStock, g.ReorderLevel })
                    .ToList();
            return (IEnumerable<Product>)reOrd;

        }

    }
}
