﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication5.DAL.Repositories
{
    public class CustomerRepository : SqlRepositoryBase<Customer>, ICustomerRepository
    {
        private readonly NORTHWNDContext _context;
        public CustomerRepository(NORTHWNDContext dbcontext) : base(dbcontext)
        {

        }

        public IQueryable<Customer> GetCustomers(CustomerFilterModel filterModel)
        {
            var query = _context.Customers.AsQueryable();

            if (filterModel.CustomerId != null)
            {
                query = query.Where(x => x.CustomerId == filterModel.CustomerId);
            }

            if (!string.IsNullOrEmpty(filterModel.ContactName))
            {
                query = query.Where(x => x.ContactName.ToUpper().Contains(filterModel.ContactName.ToUpper()));
            }           
            return query;
        }
       

        public IEnumerable<CustomerFilterModel> GetNullFirst()
        {
            Array customersbyRegion = null;
            var temp = _context.Customers
                    .OrderBy(g => g.Region)
                    .ThenBy(g => g.CustomerId)
                    .Select(g => new { g.CompanyName, g.Region })
                    .ToArray();
            var temp1 = temp.Where(g => g.Region == null).ToList();
            var temp2 = temp.Where(g => g.Region != null).ToList();
            foreach (var item in temp2)
            {
                temp1.Add(item);
               
            }

            return (IEnumerable<CustomerFilterModel>)customersbyRegion;
        }

        public IEnumerable<CustomerFilterModel> GetTotalCustomersPerCity()
        {
            var perC = _context.Customers
                 .GroupBy(g => new { g.Country, g.City })
                 .Select(g => new TotalCustomerPerCityModel
                 {
                     Country = g.Key.Country,
                     City = g.Key.City,
                     Count = g.Count()
                 })
                 .OrderByDescending(g => g.City);
                 
            return (IEnumerable<CustomerFilterModel>)perC.ToList();
        }

    }
}
