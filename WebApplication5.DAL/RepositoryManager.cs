﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;

namespace WebApplication5.DAL.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly NORTHWNDContext _dbContext;

        public RepositoryManager(NORTHWNDContext dbContext)
        {
            _dbContext = dbContext;
        }

       private  ICustomerRepository _customers;
       public ICustomerRepository Customers => _customers ?? (_customers = new CustomerRepository(_dbContext));

        private IOrderRepository _orders;
        public IOrderRepository Orders => _orders ?? (_orders = new OrderRepository(_dbContext));

        private IProductRepository _products;
        public IProductRepository Products => _products ?? (_products = new ProductRepository(_dbContext));


        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_dbContext, isolation);
        }
    }
}
