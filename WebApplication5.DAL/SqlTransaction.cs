﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Abstractions;

namespace WebApplication5.DAL
{
     public class SqlTransaction : ISqlTransaction
    {
        private readonly IDbContextTransaction _transaction;

        private SqlTransaction(DbContext context, System.Data.IsolationLevel level)
        {
            _transaction = context.Database.BeginTransaction(level);
        }

        public static ISqlTransaction Begin(DbContext context, System.Data.IsolationLevel level)
        {
            return new SqlTransaction(context, level);
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _transaction.Dispose();
            }
        }

        public void RollBack()
        {
            _transaction.Rollback();
        }
    }
}
