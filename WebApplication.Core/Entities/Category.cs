﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication.Core.Entities
{
    public partial class Category
    {
        public Category()
        {
            Product1s = new HashSet<Product>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }

        public virtual ICollection<Product> Product1s { get; set; }
    }
}
