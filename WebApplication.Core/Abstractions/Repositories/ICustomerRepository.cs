﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication.Core.Abstractions.Repositories
{
    public interface ICustomerRepository:ISqlRepository<Customer>
    {
        
        IEnumerable<CustomerFilterModel> GetNullFirst();
        IEnumerable<CustomerFilterModel> GetTotalCustomersPerCity();

    }
}
