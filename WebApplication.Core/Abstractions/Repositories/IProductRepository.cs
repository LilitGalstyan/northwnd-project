﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication.Core.Abstractions.Repositories
{
    public interface  IProductRepository :  ISqlRepository<Product>
    {
        IEnumerable<Product> GetTotalProductsInEachCategory();
        IEnumerable<Product> GetReorderedProducts();
        IEnumerable<Product> GetReordered();


    }
}
