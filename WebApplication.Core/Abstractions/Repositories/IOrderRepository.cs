﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication.Core.Abstractions.Repositories
{
    public interface  IOrderRepository:ISqlRepository<Order>
    {
        IEnumerable<OrderFilterModel> HighFreightCharges();
        IEnumerable<OrderFilterModel> HighFreightChargesLastYear();
    }
}
