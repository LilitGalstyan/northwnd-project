﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication.Core.Abstractions.Repositories
{
    public interface ISqlRepository<T>
         where T : class
    {
       

            Task<IEnumerable<T>> AllIncludingAsync(Expression<Func<T, bool>> whereProperties, params Expression<Func<T, object>>[] includeProperties);
            T Add(T entity);
            IEnumerable<T> AddRange(IEnumerable<T> entities);
            void Update(T entity);
            void UpdateRange(IEnumerable<T> entities);
            void Remove(T entity);
            void RemoveRange(IEnumerable<T> entities);
            void RemoveWhere(Expression<Func<T, bool>> predicate);
            Task<IList<T>> GetAllAsync();
            Task<IList<T>> FindSortedAsync<TSr>(Expression<Func<T, bool>> predicate, Expression<Func<T, TSr>> sortBy, int skip = 0, int? take = null);
            Task<IList<T>> FindAsync(Expression<Func<T, bool>> predicate, int skip = 0, int? take = null);
            Task<IList<TProp>> FindAndSelectAsync<TProp>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProp>> selectProperty, int skip = 0, int? take = null);
            Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate);
            Task<IList<TProp>> GetAndSelectSingle<TProp>(Expression<Func<T, bool>> predicate, Expression<Func<T, TProp>> selectProperty);
            Task<T> GetSingleWithInclude(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] include);
            int GetCount(Expression<Func<T, bool>> filter = null);
            Task<int> GetCountAsync(Expression<Func<T, bool>> filter = null);
            bool IsAny(Expression<Func<T, bool>> predicate);
            bool IsAll(Expression<Func<T, bool>> predicate);
            Task<bool> IsAnyAsync(Expression<Func<T, bool>> predicate);
            Task<bool> IsAllAsync(Expression<Func<T, bool>> predicate);
            void FilterAsync(Expression<Func<T, bool>> predicate);
           // Task<IFilterResult<T>> FilterAsync(IAsyncSqlFilter<T> filterBase);
            TResult Min<TResult>(Expression<Func<T, bool>> condition, Expression<Func<T, TResult>> predicate);
            Task<TResult> MinAsync<TResult>(Expression<Func<T, bool>> condition, Expression<Func<T, TResult>> predicate);
        }
    }




