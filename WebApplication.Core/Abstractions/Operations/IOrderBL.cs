﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication.Core.Abstractions
{
    public interface IOrderBl
    {
        IEnumerable<OrderFilterModel> HighFreightCharges();
        IEnumerable<OrderFilterModel> HighFreightChargesLastYear();
        
    }
}
