﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication.Core.Abstractions
{
    public interface IProductBL
    {
        IEnumerable<ProductFilterModel> GetTotalProductsInEachCategory();
        IEnumerable<ProductFilterModel> GetReorderedProducts();
        IEnumerable<ProductFilterModel> GetReordered();
    
    }
}
