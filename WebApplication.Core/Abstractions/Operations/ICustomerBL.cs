﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;

namespace WebApplication5.Core.Abstractions
{
    public interface ICustomerBL
    {      
      
        IEnumerable<CustomerFilterModel> GetTotalCustomersPerCity();
        IEnumerable<CustomerFilterModel> GetNullFirst();
        
    }
}
