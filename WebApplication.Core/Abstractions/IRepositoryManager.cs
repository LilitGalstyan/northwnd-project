﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication.Core.Abstractions.Repositories;

namespace WebApplication.Core.Abstractions
{
    public interface IRepositoryManager
    {

        ICustomerRepository Customers { get; }
        IProductRepository Products { get; }
        IOrderRepository Orders { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();

        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
    }
}
