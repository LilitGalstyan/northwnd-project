﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Core.Models
{
    public class CreateCustomerModel
    {
       
        public string ContactName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
