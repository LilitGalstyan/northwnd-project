using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication5.BLL.Operations;
using WebApplication5.Core.Abstractions;
using WebApplication5.DAL;
using WebApplication5.DAL.Repositories;
using WebApplication5.Middlewares;

namespace WebApplication5
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<NORTHWNDContext>(
                x => x.UseSqlServer(Configuration.GetConnectionString("default")));

            services.AddScoped<IProductBL, ProductBL>();
            services.AddScoped<ICustomerBL, CustomerBL>();
            services.AddScoped<IOrderBl, OrderBL>();

            services.AddScoped<IRepositoryManager, RepositoryManager>();
            services.AddSwaggerGen();


            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(x =>
                    {
                        x.Events.OnRedirectToLogin = (context) =>
                        {
                            context.Response.StatusCode = 401;
                            return Task.CompletedTask;
                        };
                        x.Events.OnRedirectToAccessDenied = (context) =>
                        {
                            context.Response.StatusCode = 403;
                            return Task.CompletedTask;
                        };
                    });
           

        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    } }
