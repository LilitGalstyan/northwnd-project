﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.Core.Entities;
using WebApplication5.Core.Abstractions;

namespace WebApplication5.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;
        public CustomersController(ICustomerBL customerBL)
        {
            _customerBL = customerBL;
        }

        [HttpGet("totalCustomersPerCity")]
        public IActionResult GetCountofCustomers()
        {
            object count = _customerBL.GetTotalCustomersPerCity();
            return Ok(count);
        }

        [HttpGet("nullFirst")]
        public IActionResult GetListbyRegion()
        {
            var list = _customerBL.GetNullFirst();
            return Ok(list);           
        }
       
    }
}
