﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication.Core.Abstractions;

namespace WebApplication5.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductBL _productBL;

        public ProductsController(IProductBL productBL)
        {
            _productBL = productBL;
        }
        [HttpGet("eachCategory")]
        public IActionResult GetTotalProductsInEachCategory()
        {

            var products = _productBL.GetTotalProductsInEachCategory();
            return Ok(products);
        } 

        [HttpGet("unitsInStock")]
        public IActionResult GetReorderedProducts()
        {
            var productsreordered = _productBL.GetReorderedProducts();
            return Ok(productsreordered);
        }
        [HttpGet("unitsInStockOnOrder")]
        public IActionResult GetReordered()
        {

            var reordered = _productBL.GetReordered();
            return Ok(reordered);
        }
        

    }
}
