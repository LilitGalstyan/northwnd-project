﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using WebApplication.Core.Abstractions;

namespace WebApplication5.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderBl _orderBl;

        public OrdersController(IOrderBl orderBl)
        {
            _orderBl = orderBl;
        }



        [HttpGet("highFreightCharges")]
        public IActionResult HighFreightCharges()
        {
            var highFCh = _orderBl.HighFreightCharges();
            return Ok(highFCh);
        }

        
        [HttpGet("highFreightChargesLastYear")]
        public IActionResult HighFreightChargesLastYear()
        {
            var hfLastYear = _orderBl.HighFreightChargesLastYear();
            return Ok(hfLastYear);

        }
        
    }
}
