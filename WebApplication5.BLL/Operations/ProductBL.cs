﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;
using WebApplication5.DAL;

namespace WebApplication5.BLL.Operations
{
    public class ProductBL : IProductBL
    {
        private readonly IRepositoryManager _repositoryManager;
        private ILogger<ProductBL> _logger;

        public ProductBL(IRepositoryManager repositoryManager,ILogger<ProductBL> logger)
        {
           _repositoryManager = repositoryManager;
            _logger = logger;
        }

        public IEnumerable<ProductFilterModel> GetTotalProductsInEachCategory()
        {
            return (IEnumerable<ProductFilterModel>)_repositoryManager.Products.GetTotalProductsInEachCategory();
        }
        public IEnumerable<ProductFilterModel> GetReorderedProducts()
        {
            return (IEnumerable<ProductFilterModel>)_repositoryManager.Products.GetReorderedProducts();

        }
        public IEnumerable<ProductFilterModel> GetReordered()
        {
            return (IEnumerable<ProductFilterModel>)_repositoryManager.Products.GetReordered();
        }
    }
}
