﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication.Core.Exceptions;
using WebApplication.Core.Models;
using WebApplication5.Core.Abstractions;
using WebApplication5.DAL;

namespace WebApplication5.BLL.Operations
{
    public class CustomerBL : ICustomerBL
    {
        private readonly IRepositoryManager _context;
        private readonly ILogger<CustomerBL> _logger;
        public CustomerBL(IRepositoryManager context, ILogger<CustomerBL> logger)
        {
            _logger = logger;
            _context = context;
        }



        public IEnumerable<CustomerFilterModel> GetNullFirst()
        {
            return (IEnumerable<CustomerFilterModel>)_context.Customers.GetNullFirst();
        }



        public IEnumerable<CustomerFilterModel> GetTotalCustomersPerCity()
        {
            _logger.LogInformation("Add Customer Method Started");
            return (IEnumerable<CustomerFilterModel>)_context.Customers.GetTotalCustomersPerCity();
        }






    }
}
