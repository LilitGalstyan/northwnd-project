﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Core.Abstractions;
using WebApplication.Core.Abstractions.Repositories;
using WebApplication.Core.Entities;
using WebApplication.Core.Models;
using WebApplication5.DAL;

namespace WebApplication5.BLL.Operations
{
    public class OrderBL : IOrderBl
    {
        private readonly IRepositoryManager _context;
        private ILogger<OrderBL> _logger;

        public OrderBL(IRepositoryManager context,ILogger<OrderBL> logger)
        {
            _context = context;
            _logger = logger;
        }

        
        public IEnumerable<OrderFilterModel>  HighFreightCharges()
        {
            return _context.Orders.HighFreightCharges();

        }
        public IEnumerable<OrderFilterModel> HighFreightChargesLastYear()
        {
            return _context.Orders.HighFreightChargesLastYear();
        }
    }
}
